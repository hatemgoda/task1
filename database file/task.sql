-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 02, 2021 at 03:59 PM
-- Server version: 8.0.18
-- PHP Version: 7.2.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `task`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `created_at`, `updated_at`, `deleted_at`, `user_id`, `post_id`, `parent`) VALUES
(2, 'hatemdfgddsfsdffdf', '2021-01-02 09:45:21', '2021-01-02 12:50:34', '2021-01-02 12:50:34', 1, 5, 0),
(12, 'cvbcvbfff3443', '2021-01-02 11:45:59', '2021-01-02 12:50:34', '2021-01-02 12:50:34', 1, 5, 0),
(13, 'xvcxv66666666666', '2021-01-02 11:51:53', '2021-01-02 12:50:34', '2021-01-02 12:50:34', 1, 5, 2),
(14, 'sdfds', '2021-01-02 12:40:55', '2021-01-02 12:40:55', NULL, 1, 15, 0),
(15, 'rerer', '2021-01-02 13:14:51', '2021-01-02 13:14:51', NULL, 1, 6, 0),
(16, 'rerer77777', '2021-01-02 13:15:16', '2021-01-02 13:22:12', '2021-01-02 13:22:12', 1, 6, 0),
(17, 'rerer77777', '2021-01-02 13:19:05', '2021-01-02 13:29:42', '2021-01-02 13:29:42', 1, 6, 0),
(18, 'rerer77777', '2021-01-02 13:19:39', '2021-01-02 13:19:39', NULL, 1, 6, 0),
(19, 'rerer77777', '2021-01-02 13:22:28', '2021-01-02 13:22:28', NULL, 1, 6, 0),
(20, 'rerer77777', '2021-01-02 13:22:29', '2021-01-02 13:22:29', NULL, 1, 6, 0),
(21, 'rerer77777', '2021-01-02 13:23:31', '2021-01-02 13:23:31', NULL, 1, 6, 0),
(22, 'rerer77777', '2021-01-02 13:24:28', '2021-01-02 13:24:28', NULL, 1, 6, 0),
(23, 'rerer77777', '2021-01-02 13:24:36', '2021-01-02 13:24:36', NULL, 1, 6, 0),
(24, 'rerer77777', '2021-01-02 13:25:37', '2021-01-02 13:25:37', NULL, 1, 6, 0),
(25, 'aaa', '2021-01-02 13:44:58', '2021-01-02 13:44:58', NULL, 1, 19, 0),
(26, 'bbbbbbbbbbbbbbbbb', '2021-01-02 13:45:04', '2021-01-02 13:45:04', NULL, 1, 19, 25),
(27, 'werwerwerwer', '2021-01-02 13:46:01', '2021-01-02 13:46:01', NULL, 1, 19, 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2021_01_02_162151_create_comments_table', 1),
(2, '2021_01_02_162151_create_password_resets_table', 1),
(3, '2021_01_02_162151_create_posts_table', 1),
(4, '2021_01_02_162151_create_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `text`, `created_at`, `updated_at`, `deleted_at`, `user_id`) VALUES
(1, ' One morning, when Gregor Samsa', ' Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web\r\n\r\nOne morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. \"What\'s happened to me?\" he thought. It wasn\'t a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls. A collection of textile samples lay spread out on the table - Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with a fur hat and fur boa who sat upright, raising a heavy fur muff that covered the whole of her lower arm towards the viewer. Gregor then turned to look out the window at the dull weather. Drops\r\n\r\nFar far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.\r\n\r\nThe Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek,\r\n\r\nTwo driven jocks help fax my big quiz. Quick, Baz, get my woven flax jodhpurs! \"Now fax quiz Jack!\" my brave ghost pled. Five quacking zephyrs jolt my wax bed. Flummoxed by job, kvetching W. zaps Iraq. Cozy sphinx waves quart jug of bad milk. A very bad quack might jinx zippy fowls. Few quips galvanized the mock jury box. Quick brown dogs jump over the lazy fox. The jay, pig, fox, zebra, and my wolves quack! Blowzy red vixens fight for a quick jump. Joaquin Phoenix was gazed by MTV for luck. A wizard’s job is to vex chumps quickly in fog. Watch \"Jeopardy!\", Alex Trebek\'s fun TV quiz game. Woven silk pyjamas exchanged for blue quartz. Brawny gods just\r\n\r\nBrick quiz whangs jumpy veldt fox. Bright vixens jump; dozy fowl quack. Quick wafting zephyrs vex bold Jim. Quick zephyrs blow, vexing daft Jim. Sex-charged fop blew my junk TV quiz. How quickly daft jumping zebras vex. ', '2020-12-20 16:40:17', '2020-12-20 16:40:17', NULL, 1),
(3, 'It showed a lady fitted out with', '                                The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of.', '2020-12-20 16:44:53', '2020-12-20 16:44:53', NULL, 1),
(4, ' The bedding was hardly able to cover', ' A collection of textile samples lay spread out on the table - Samsa was a travelling salesman.', '2020-01-11 20:00:00', NULL, NULL, 1),
(5, 'nice title5556', 'nice body', '2020-01-19 20:00:00', '2021-01-02 12:50:34', '2021-01-02 12:50:34', 1),
(6, 'nice title6666', 'nice body', '2020-01-19 20:00:00', '2021-01-02 13:09:06', NULL, 1),
(20, 'nice title', 'nice body', '2021-01-02 13:23:19', '2021-01-02 13:23:19', NULL, 1),
(7, 'I hear the buzz of the little world ', 'To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses', '2020-01-12 20:00:00', NULL, NULL, 1),
(8, ' One morning, when Gregor Samsa', 'English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web                            ', '2020-12-20 16:40:17', '2020-12-20 16:40:17', NULL, 1),
(9, 'It showed a lady fitted out with', ' Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web\r\n\r\nOne morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. \"What\'s happened to me?\" he thought. It wasn\'t a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls. A collection of textile samples lay spread out on the table - Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with a fur hat and fur boa who sat upright, raising a heavy fur muff that covered the whole of her lower arm towards the viewer. Gregor then turned to look out the window at the dull weather. Drops\r\n\r\nFar far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.\r\n\r\nThe Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek,\r\n\r\nTwo driven jocks help fax my big quiz. Quick, Baz, get my woven flax jodhpurs! \"Now fax quiz Jack!\" my brave ghost pled. Five quacking zephyrs jolt my wax bed. Flummoxed by job, kvetching W. zaps Iraq. Cozy sphinx waves quart jug of bad milk. A very bad quack might jinx zippy fowls. Few quips galvanized the mock jury box. Quick brown dogs jump over the lazy fox. The jay, pig, fox, zebra, and my wolves quack! Blowzy red vixens fight for a quick jump. Joaquin Phoenix was gazed by MTV for luck. A wizard’s job is to vex chumps quickly in fog. Watch \"Jeopardy!\", Alex Trebek\'s fun TV quiz game. Woven silk pyjamas exchanged for blue quartz. Brawny gods just\r\n\r\nBrick quiz whangs jumpy veldt fox. Bright vixens jump; dozy fowl quack. Quick wafting zephyrs vex bold Jim. Quick zephyrs blow, vexing daft Jim. Sex-charged fop blew my junk TV quiz. How quickly daft jumping zebras vex. ', '2020-12-20 16:44:53', '2020-12-20 16:44:53', NULL, 1),
(10, ' The bedding was hardly able to cover', ' A collection of textile samples lay spread out on the table - Samsa was a travelling salesman.', '2020-01-11 20:00:00', NULL, NULL, 1),
(11, 'Junk MTV quiz graced by fox whelps.66666', 'Alex Trebek\'s fun TV quiz game. Woven silk pyjamas exchanged for blue quartz. Brawny gods just zebra, and my wolves quack! Blowzy red vixens fight for a quick.', '2020-01-19 20:00:00', '2021-01-02 09:54:56', NULL, 1),
(12, 'To take a trivial example, which', 'I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms of the insects and flies, then I feel the presence of the Almighty, who formed us in his own image, and the breath Gregor then turned to look out the window at the dull weather. ', '2020-01-19 20:00:00', NULL, NULL, 1),
(13, 'I hear the buzz of the little world ', 'To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses', '2020-01-12 20:00:00', NULL, NULL, 1),
(14, 'nice title', 'nice body', '2021-01-02 04:23:17', '2021-01-02 04:23:17', NULL, 2),
(15, 'nice title', 'nice body', '2021-01-02 04:23:46', '2021-01-02 04:23:46', NULL, 2),
(17, 'I hear the buzz of the little world', 'I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms of the insects and flies, then I feel the presence of the Almighty, who formed us in his own image, and the breath Gregor then turned to look out the window at the dull weather.', '2021-01-02 10:07:57', '2021-01-02 10:07:57', NULL, 1),
(18, 'dfgdf', 'dfg', '2021-01-02 10:12:46', '2021-01-02 12:38:55', '2021-01-02 12:38:55', 1),
(19, 'nice title', 'nice body', '2021-01-02 12:43:55', '2021-01-02 12:43:55', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `forget_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `forget_code`) VALUES
(1, 'Admin', 'admin@admin.com', NULL, '$2y$10$7uZYHrjjKXL5lS2sFSxVNujH0Hi6LB9.fUwNEMK4HJwbyTIzEJcW6', NULL, NULL, '2020-11-10 13:19:01', NULL, NULL),
(2, 'sss', 'hatem@gmail.com', NULL, '$2y$10$7uZYHrjjKXL5lS2sFSxVNujH0Hi6LB9.fUwNEMK4HJwbyTIzEJcW6', NULL, '2021-01-01 14:47:47', '2021-01-01 14:47:47', NULL, NULL),
(14, 'dddddddd', 'sasas@dfsf.com', NULL, '$2y$10$WJNuV4howb.AF3esV.n8be112jOq0Y/oEIIujfH/jgUKYJG5Zin1G', NULL, '2021-01-02 03:17:25', '2021-01-02 03:17:25', NULL, NULL),
(15, 'hatem', 'hatem@hatem.com', NULL, '$2y$10$6gDtt09ibw.QhTUhNcGasenjBVd2ayAV6LDn7s8w.TDAgReRl2Tfu', NULL, '2021-01-02 03:40:16', '2021-01-02 03:40:16', NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
