$('div.alert').not('.alert-important').delay(3000).fadeOut(350);

// $(".delete").on("submit", function () {
//     // alert("hatem");
//     // return  Lobibox.confirm({
//     //     msg: "Are you sure you want to delete this user?",
//     // });
//     return confirm('هل انت متاكد من الحذف ؟');
//     // return confirm("Are you sure?");
// });
alertify.defaults.glossary.title = 'Confirmation Message';
alertify.defaults.glossary.ok = 'Yes';
alertify.defaults.glossary.cancel = 'No';
$(".delete").on("submit", function (event) {
    event.preventDefault();
    // console.log($(this)[0]);
    var form = $(this)[0];

    // alert("hatem");
    // return confirm('هل انت متاكد من الحذف ؟');
    alertify.confirm('Are you sure ?', function (e) {
        if (e) {
            // alert(1);
            form.submit();
            // $(this)[0].submit();
            return true;
        } else {
            return false;
        }
    });
    //
    // Run
    // Overloads
    // return confirm("Are you sure?");
});


$('.select2').select2({
    // dir: "rtl"
});
var currentDate = new Date();
$(".datepicker").datepicker({
    // dateFormat: 'yy-mm-dd'
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    // setDate: currentDate,
    // regional: "ar" ,
    // yearRange: “c-70:c+10”,
    isRTL: true,
    changeYear: true,

})
    // .datepicker("setDate", new Date())
;

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

$(".floatonly").keypress(function (event) {
    //if the letter is not digit then display error and don't type anything
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        //display error message
        // console.log( $(this).next().next());
        $(this).next().text("برجاء ادخال ارقام فقط").show().fadeOut(2000);
        return false;
    }
});
$(".numberonly").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        // console.log( $(this).next().next());
        $(this).next().text("برجاء ادخال ارقام فقط").show().fadeOut(2000);
        return false;
    }
});
$(".numberonly2").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        // console.log( $(this).next().next());
        // $(this).next().text("برجاء ادخال ارقام فقط").show().fadeOut(2000);
        return false;
    }
});

$(document).on('click', '.destroy', function () {
    var route = $(this).data('route');
    var token = $(this).data('token');
    $.confirm({
        icon: 'glyphicon glyphicon-floppy-remove',
        animation: 'rotateX',
        closeAnimation: 'rotateXR',
        title: 'تأكد عملية الحذف',
        autoClose: 'cancel|6000',
        text: 'هل أنت متأكد من الحذف ؟',
        confirmButtonClass: 'btn-outline',
        cancelButtonClass: 'btn-outline',
        confirmButton: 'نعم',
        cancelButton: 'لا',
        dialogClass: "modal-danger modal-dialog",
        confirm: function () {
            $.ajax({
                url: route,
                type: 'post',
                data: {_method: 'delete', _token: token},
                dataType: 'json',
                success: function (data) {
                    if (data.status == 0) {
                        //toastr.error(data.msg)
                        swal({
                            title: "خطأ!",
                            text: data.msg,
                            type: "error",
                            confirmButtonText: "حسناً"
                        });
                    } else {
                        $("#removable" + data.id).remove();
                        swal({
                            title: "نجحت العملية!",
                            text: data.msg,
                            type: "success",
                            confirmButtonText: "حسناً"
                        });
                        //toastr.success(data.msg)
                    }
                }
            });
        },
    });
});