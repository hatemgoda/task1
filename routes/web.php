<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');
Route::get('/login', 'AuthController@showLoginForm')->name('showLoginForm');
Route::post('/login', 'AuthController@login')->name('login');
Route::get('/register', 'AuthController@showRegisterForm')->name('showRegisterForm');
Route::post('/register', 'AuthController@register')->name('register');
Route::get('/logout', 'AuthController@logout');
Route::get('post/{id}', 'PostController@show')->name('showPost');

Route::group(['middleware' => ['auth:web']], function () {



    Route::get('create-post', 'PostController@create')->name('createPost');
    Route::post('save-post', 'PostController@store')->name('savePost');
    Route::get('edit-post/{id}', 'PostController@edit')->name('editPost');
    Route::post('update-post/{id}', 'PostController@update')->name('updatePost');
    Route::post('delete-post/{id}', 'PostController@destroy')->name('deletePost');
    Route::post('add-comment', 'CommentController@addComment')->name('addComment');
    Route::post('delete-comment/{id}', 'CommentController@deleteComment')->name('deleteComment');
    Route::post('update-comment', 'CommentController@updateComment')->name('updateComment');
//    Route::resource('post', 'PostController');

});
