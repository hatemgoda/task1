<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//
//
//});

Route::group(['middleware' => 'auth:api'], function () {


    Route::get('posts', 'PostController@posts');
    Route::get('post/{id}', 'PostController@post');
    Route::post('save-post', 'PostController@savePost');
    Route::post('update-post/{id}', 'PostController@updatePost');
    Route::post('delete-post/{id}', 'PostController@deletePost');
    Route::post('add-comment', 'CommentController@addComment');
    Route::post('delete-comment/{id}', 'CommentController@deleteComment');
    Route::post('update-comment', 'CommentController@updateComment');
});
