@extends('website.layouts.main')

@section('content')

    <div class="wrapper wrapper-content  animated fadeInRight article">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="ibox">
                    <div class="ibox-content">

                        <div class="text-center article-title">
                            <span class="text-muted"><i class="fa fa-clock-o"></i> {{$row->created_at->diffForHumans()}}</span>
                            <h1>
                                {{$row->title}}
                            </h1>
                        </div>
                        <p>
                            {{$row->text}}
                        </p>

                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                {{--<h5>Tags:</h5>--}}
                                {{--<button class="btn btn-primary btn-xs" type="button">Model</button>--}}
                                {{--<button class="btn btn-white btn-xs" type="button">Publishing</button>--}}
                            </div>
                            <div class="col-md-6">
                                <div class="small text-right">
                                    {{--<h5>Stats:</h5>--}}
                                    <div><i class="fa fa-comments-o"> </i> {{$row->comments_count}} comments</div>
                                    {{--<i class="fa fa-eye"> </i> 144 views--}}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">

                                <h2>Comments:</h2>
                                <span class="bg-danger"> i can make it infinite replies using recursive idea but i comment recursive code to make task simple and cuz u don't ask that in task file so feel free to uncomment code in comment blade file   </span>
                                {{--@include('partials.comments', ['comments' => $row->comments, 'post_id' => $row->id])--}}
                                @foreach( $row->comments as $comment)

                                    <div class="social-feed-box" style="position: relative;">
                                        @if(auth()->check() and (auth()->user()->id== $comment->user_id ))
                                        {{Form::open(array('method'=>'post','class'=>'delete pull-right','url'=>route('deleteComment',$comment->id) )) }}
                                        <button class="btn  btn-outline" type="submit"
                                                style="position: absolute; top: 0px; right: 2px; color: red "><i
                                                    class="fa fa-trash-o"></i></button>
                                        {{Form::close()}}
                                        <button class="btn  btn-outline" type="button"
                                                onclick="editComment('{{$comment->comment}}',' {{$comment->id}}')"
                                                style="position: absolute; top: 35px; right: 0px; color: #1ab394 "><i
                                                    class="fa fa-edit"></i></button>

                                        @endif
                                        <div class="social-avatar">
                                            <a href="#" class="pull-left">
                                                <img alt="image" src="{{asset('assets/admin/img/a1.jpg')}}">
                                            </a>
                                            <div class="media-body">
                                                <a href="#">
                                                    {{$comment->user->name ?? '...'}}
                                                </a>
                                                <small class="text-muted">{{$comment->created_at->diffForHumans()}}</small>
                                            </div>
                                        </div>
                                        <div class="social-body">
                                            <p contentEditable="true">
                                                {{$comment->comment}}
                                            </p>
                                            @include('partials.comments', ['comments' => $comment->comments, 'post_id' => $row->id])
                                            @if(auth()->check())
                                                <form method="post" action="{{ route('addComment') }}">
                                                    @csrf
                                                    <div class="form-group">
                                                        <input type="text" name="comment" class="form-control"
                                                               required/>
                                                        <input type="hidden" name="post_id" value="{{$row->id}}"/>
                                                        <input type="hidden" name="parent" value="{{$comment->id}}"/>
                                                        <input type="hidden" name="user_id"
                                                               value="{{auth()->user()->id}}"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="submit" class="btn btn-primary"
                                                               value="Add Replay"/>
                                                    </div>
                                                </form>

                                            @endif

                                        </div>
                                    </div>
                                @endforeach
                                @if(auth()->check())
                                    <form method="post" action="{{ route('addComment') }}">
                                        @csrf
                                        <div class="form-group">
                                            <input type="text" name="comment" class="form-control" required/>
                                            <input type="hidden" name="post_id" value="{{$row->id}}"/>
                                            <input type="hidden" name="parent" value="0"/>
                                            <input type="hidden" name="user_id" value="{{auth()->user()->id}}"/>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-primary" value="Add Comment"/>
                                        </div>
                                    </form>

                                @endif

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>


    </div>

    @include('website.post.modal')
    @include('website.post.script')
@stop