

<div class="modal inmodal" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounce">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                <i class="fa fa-comment modal-icon"></i>
                <h4 class="modal-title">Edit comment</h4>

            </div>
            {!! Form::open([
                           'action'=>'Website\CommentController@updateComment',


                           'id'=>'updateForm',

                           'method'=>'POST'
                           ])!!}
            <div class="modal-body">


                <input type="hidden" name="id" id="commentId" value="">

                <div class="form-group">

                    <label for="reason">Comment</label>
                    {{Form::textarea('comment',null,[
                           'class'=>'form-control',
                           'id'=>'commentText',
                            'rows'=>2,
                           'placeholder' => 'Comment'
                           ])}}
                    <span class="help-block"><strong></strong></span>
                </div>



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary pull-right"
                        onclick="this.disabled=true;this.value='Saving ....';this.form.submit();">Save
                </button>
            </div>
            {!! Form::close()!!}
        </div>
    </div>
</div>

