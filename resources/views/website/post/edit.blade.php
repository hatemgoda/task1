@extends('website.layouts.main')

@section('content')
        <!-- FILE: app/views/start.blade.php -->
<div class="ibox">
    <div class="ibox-title">
        <h5>Posts |  Update </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
            <a class="close-link">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
    <!-- form start -->
    {!! Form::model($model,[
                            'action'=>['Website\PostController@update',$model->id],
                            'id'=>'myForm',
                            'role'=>'form',
                              'files'=>'true',
                            'method'=>'POST'
                            ])!!}
    <div class="ibox-content">
        @include('website.post.form')
    </div>
    <div class="ibox-footer">
        <div class="row">
            <div class="col-sm-6 col-sm-push-2  col-xs-6">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <div class="col-sm-6  col-xs-6">
                <button type="reset" class="btn btn-danger">Cancel</button>
            </div>
        </div>
    </div>
    {!! Form::close()!!}
</div>
@stop