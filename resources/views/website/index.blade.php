@extends('website.layouts.main')
@section('content')
    @inject('user','App\User')

    <?php

    $users = $user->latest()->pluck('name', 'id')->toArray();

    ?>
    @push('style')
        <style>

            .post--text {
                overflow: hidden;
                text-overflow: ellipsis;
                display: -webkit-box;
                -webkit-line-clamp: 6;
                -webkit-box-orient: vertical;
            }
        </style>
    @endpush

    <div class="wrapper wrapper-content  animated fadeInRight blog">
        <div class="ibox-content m-b-sm border-bottom">

            {!! Form::open([
                  'method' => 'GET'
              ]) !!}

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="user_id">User Name</label>
                        {{Form::select('client_id', $users, request()->user_id, [
                            "class" => "form-control select2 " ,
                            "id" => "client_id",
                            "placeholder" => "User Name"
                        ])}}
                        {{--<input type="text" id="status" name="status" value="" placeholder="Status" class="form-control">--}}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="title">Title</label>
                        <input type="text" id="title" name="title" value="{{request()->title}}" placeholder="Title"
                               class="form-control">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group ">
                        <label class="control-label" for="date">Date</label>
                        <div class="input-group date">

                            {!! Form::text('date',request()->date,[
                                'class' => 'form-control datepicker',
                                'placeholder' => 'Date'
                            ]) !!}
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">


                <div class="clearfix"></div>
                <div class="row ">
                    <div class="col-sm-2  ">
                        <div class="form-group">
                            <label for="">&nbsp;</label>
                            <button type="submit" class="btn btn-flat  btn-primary btn-md">Search</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="row">
            @if($rows->count()>0)

                @foreach($rows as $key=>$row)
                    <?php

                    $rem = $key % 3;
                    //                    echo $rem;
                    ?>

                    <div class="col-lg-4">

                        <div class="ibox">
                            <div class="ibox-title">
                                @if(auth()->check() and (auth()->user()->id== $row->user_id ))
                                    <a class="btn btn-primary btn-xs" href="{{route('editPost',$row->id)}}"><i class="fa fa-edit"></i> Edit
                                    </a>
                                    {{Form::open(array('method'=>'post','class'=>'delete pull-right','url'=>route('deletePost',$row->id) )) }}
                                    <button class="btn btn-danger btn-xs " type="submit"><i class="fa fa-trash-o"></i>
                                        Delete
                                    </button>
                                    {{Form::close()}}
                                @endif
                            </div>
                            <div class="ibox-content">
                                <a href="{{url('post/'.$row->id)}}" class="btn-link">
                                    <h2>
                                        {{$row->title}}
                                    </h2>
                                </a>
                                <div class="small m-b-xs">
                                    <strong>{{$row->user->name ?? '...'}}</strong> <span class="text-muted"><i
                                                class="fa fa-clock-o"></i> {{$row->created_at->diffForHumans()}}</span>
                                </div>
                                <p class="post--text">
                                    {{$row->text}}
                                </p>
                                <div class="row">

                                    <div class="col-md-6">


                                        <div><i class="fa fa-comments-o"> </i> {{$row->comments_count}} comments</div>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    @if($rem==2)
                        <div class="clearfix"></div>
                    @endif
                @endforeach
                <div class="clearfix"></div>
                <div class="text-center">
                    {!! $rows->appends(request()->except('page'))->links() !!}
                </div>
            @else
                <h2 class="text-center">لا يوجد بيانات</h2>
            @endif
        </div>


    </div>



@stop


