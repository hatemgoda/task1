<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Task</title>

    <link href="{{asset('assets/admin/css/bootstrap.min.css')}}" rel="stylesheet">
{{--    <link href="{{asset('assets/admin/css/bootstrap-rtl.min.css')}}" rel="stylesheet">--}}
    <link href="{{asset('assets/admin/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/admin/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{asset('assets/admin/css/animate.css')}}" rel="stylesheet">

    <link href="{{asset('assets/admin/css/style.css')}}" rel="stylesheet">
{{--    <link href="{{asset('assets/admin/css/inspina-rtl.css')}}" rel="stylesheet">--}}
    <link href="{{asset('assets/admin/css/custom.css')}}" rel="stylesheet">

    @toastr_css
    <style>
        .system-name {


            color: #e6e6e6;
            font-size: 60px !important;
            font-weight: 800;
            /*letter-spacing: -10px;*/
            margin-bottom: 50px;
            box-sizing: border-box;
        }

        .login-text {

            font-weight: 800;
        }

    </style>
</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    {{--<div>--}}
        <div>

            <div>

                <h1 class="system-name">Task</h1>

            </div>
            {{--<h3 class="login-text" >Login</h3>--}}
            <div class="text-center">
                {{--                <img src="{{asset('assets/admin/project-image/logo.png')}}" style="margin-bottom: 15px;" height="200" width="300" alt="logo">--}}

            </div>

            {{--<p>تسجيل الدخول</p>--}}
            <form class="m-t" role="form" action="{{ route('login') }}" method="post">

                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" class="form-control" placeholder="Email" required="" name="email" value="{{old('email')}}">
                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" class="form-control" placeholder="Password" required="" name="password">
                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>


                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="{{route('register')}}">Create an account</a>
            </form>
            <p class="m-t">

                <small> Copyright Task &copy; {{date('Y')}}</small>
            </p>
        </div>
    {{--</div>--}}

    <!-- Mainly scripts -->
    <script src="{{asset('assets/admin/js/jquery-2.1.1.js')}}"></script>
    <script src="{{asset('assets/admin/js/bootstrap.min.js')}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{asset('assets/admin/js/inspinia.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/toastr/toastr.min.js')}}"></script>

    @toastr_render

</body>

</html>