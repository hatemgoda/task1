<nav class="navbar navbar-static-top" role="navigation">
    <div class="navbar-header">
        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
                class="navbar-toggle collapsed" type="button">
            <i class="fa fa-reorder"></i>
        </button>
        <a href="{{url('/')}}" class="navbar-brand brand-font">{{$website_name}}</a>
    </div>
    <div class="navbar-collapse collapse" id="navbar">
        <ul class="nav navbar-nav navbar-font">
            <li class="@if(request()->is('/')) active @endif">
                <a aria-expanded="false" role="button" href="{{url('/')}}">الرئيسية</a>
            </li>
            {{--@if(auth()->guard('client')->check())--}}
                {{--<li class="@if(request()->is('order/create')) active @endif">--}}
                    {{--<a aria-expanded="false" role="button" href="{{url('/order/create')}}">طلب جديد</a>--}}
                {{--</li>--}}
                {{--<li class="@if(request()->is('order')) active @endif">--}}
                    {{--<a aria-expanded="false" role="button" href="{{url('/order')}}">طلباتى</a>--}}
                {{--</li>--}}
                {{--<li class="@if(request()->is('profile')) active @endif">--}}
                    {{--<a aria-expanded="false" role="button" href="{{url('/profile')}}">الملف الشخصى</a>--}}
                {{--</li>--}}


            {{--@endif--}}
            @if(auth()->guard('client')->check())
            <li class="dropdown   @if( request()->is('order')  or request()->is('order/create')   or  request()->is('account-statement'))  active @endif">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown">الطلبات <span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu">
                    <li class="@if(request()->is('order/create')) active @endif">
                        <a aria-expanded="false" role="button" href="{{url('/order/create')}}">طلب جديد</a>
                    </li>
                    <li class="@if(request()->is('order')) active @endif">
                        <a aria-expanded="false" role="button" href="{{url('/order')}}">طلباتى</a>
                    </li>
                    <li class="@if(request()->is('account-statement')) active @endif">
                        <a aria-expanded="false" role="button" href="{{url('/account-statement')}}">كشف حساب</a>
                    </li>
                </ul>
            </li>

            @endif
            <li class="@if(request()->is('aboutus')) active @endif">
                <a aria-expanded="false" role="button" href="{{url('aboutus')}}">من نحن</a>
            </li>
            <li class="@if(request()->is('rules')) active @endif">
                <a aria-expanded="false" role="button" href="{{url('/rules')}}">الشروط و الاحكام</a>
            </li>
        </ul>
        <ul class="nav navbar-top-links navbar-left">
            @if(auth()->guard('client')->check())
                <?php

                $notifications = auth()->guard('client')->user()->notifications->take(3);
                $notification_count = auth()->guard('client')->user()->unreadNotifications->count();
                ?>
                    <li>
                        <a href="{{url('profile')}}">
                            <i class="fa fa-user-circle"></i>تعديل الملف الشخصى
                        </a>
                    </li>

                    <li class="dropdown notifications-area">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell"></i> <span class="label label-primary">@if($notification_count >0){{$notification_count}}@endif</span>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            @if($notifications->count()>0)
                                @foreach($notifications as $notification)
                                    <li class="@if($notification->read_at== null)  bg-muted @endif">
                                        <a href="{{url('order/'.$notification->data['order_id'].'?notification='.$notification->id)}}">
                                            <div>
                                                <i class="fa fa-truck fa-fw"></i>{{$notification->data['message']}}
                                                <span class="pull-left text-muted small">{{$notification->created_at->diffForHumans()}}</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                @endforeach
                            @endif

                            <li>
                                <div class="text-center link-block">
                                    <a href="{{url('notification')}}">
                                        <strong>عرض كل الاشعارات</strong>
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                <li>
                    <a href="{{url('logout')}}">
                        <i class="fa fa-sign-out"></i> تسجيل الخروج
                    </a>
                </li>
            @else
                <li>
                    <a href="{{url('login')}}">
                        <i class="fa fa-sign-in"></i> تسجيل الدخول
                    </a>
                </li>
            @endif
        </ul>
    </div>
</nav>