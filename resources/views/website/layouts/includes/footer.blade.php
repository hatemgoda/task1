</div>

</div>
<div class="footer">


    <div>
        <strong>Copyright </strong> Task &copy; {{date('Y')}}
    </div>
</div>

</div>
</div>


<!-- Mainly scripts -->
<script src="{{asset('assets/admin/js/jquery-2.1.1.js')}}"></script>
<script src="{{asset('assets/admin/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

<!-- Custom and plugin javascript -->
<script src="{{asset('assets/admin/js/inspinia.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins/pace/pace.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins/select2/select2.full.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins/alertifyjs/alertify.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins/toastr/toastr.min.js')}}"></script>

<script src="{{asset('assets/admin/plugins/jquery-ui-1.12.1/jquery-ui.min.js')}}"></script>

<script src="{{asset('assets/admin/js/script.js')}}"></script>
@toastr_render

@stack('script')


</body>

</html>
