<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Task</title>

    <link href="{{asset('assets/admin/css/bootstrap.min.css')}}" rel="stylesheet">
    {{--<link href="{{asset('assets/admin/css/bootstrap-rtl.min.css')}}" rel="stylesheet">--}}
    <link href="{{asset('assets/admin/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    {{--<link href="{{asset('assets/admin/fontawesome-5.14.0/css/fontawesome.css')}}" rel="stylesheet">--}}
    {{--<link href="{{asset('assets/admin/fontawesome-5.14.0/css/brands.css')}}" rel="stylesheet">--}}
    {{--<link href="{{asset('assets/admin/fontawesome-5.14.0/css/solid.css')}}" rel="stylesheet">--}}


    <link href="{{asset('assets/admin/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('assets/admin/plugins/select2/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/admin/plugins/alertifyjs/css/alertify.rtl.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/admin/plugins/alertifyjs/css/themes/default.rtl.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/admin/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
    <link rel="stylesheet"
          href="{{asset('assets/admin/plugins/jquery-ui-1.12.1/jquery-ui.min.css')}}">

    <link href="{{asset('assets/admin/css/style.css')}}" rel="stylesheet">
{{--    <link href="{{asset('assets/admin/css/inspina-rtl.css')}}" rel="stylesheet">--}}
    <link href="{{asset('assets/admin/css/custom.css')}}" rel="stylesheet">
    @toastr_css
    @stack('style')

</head>

<body class="top-navigation">

<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom white-bg">
            @include('website.layouts.includes.navbar')
        </div>
        <div class="wrapper wrapper-content">
            <div class="container">