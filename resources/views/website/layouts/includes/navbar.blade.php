<nav class="navbar navbar-static-top" role="navigation">
    <div class="navbar-header">
        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
                class="navbar-toggle collapsed" type="button">
            <i class="fa fa-reorder"></i>
        </button>
        <a href="{{url('/')}}" class="navbar-brand brand-font">Task</a>
    </div>
    <div class="navbar-collapse collapse" id="navbar">
        <ul class="nav navbar-nav navbar-font">
            <li class="@if(request()->is('/')) active @endif">
                <a aria-expanded="false" role="button" href="{{url('/')}}">Home</a>
            </li>


           
            @if(auth()->check())

                <li class="@if(request()->is('post/create')) active @endif">
                    <a aria-expanded="false" role="button" href="{{route('createPost')}}">New Post</a>
                </li>


            @endif

        </ul>
        <ul class="nav navbar-top-links navbar-right">
            @if(auth()->check())




                <li>
                    <a href="{{url('logout')}}">
                        <i class="fa fa-sign-out"></i>Log out
                    </a>
                </li>
            @else
                <li>
                    <a href="{{url('login')}}">
                        <i class="fa fa-sign-in"></i>Sign in
                    </a>
                </li>
            @endif
        </ul>
    </div>
</nav>