@foreach( $comments as $comment)
    <div class="social-feed-box" style="position: relative;">
        @if(auth()->check() and (auth()->user()->id== $comment->user_id ))
            {{Form::open(array('method'=>'post','class'=>'delete pull-right','url'=>route('deleteComment',$comment->id) )) }}
            <button class="btn  btn-outline" type="submit"
                    style="position: absolute; top: 0px; right: 2px; color: red "><i
                        class="fa fa-trash-o"></i></button>
            {{Form::close()}}
            <button class="btn  btn-outline" type="button"
                    onclick="editComment('{{$comment->comment}}',' {{$comment->id}}')"
                    style="position: absolute; top: 35px; right: 0px; color: #1ab394 "><i
                        class="fa fa-edit"></i></button>

        @endif
        <div class="social-avatar">
            <a href="" class="pull-left">
                <img alt="image" src="{{asset('assets/admin/img/a1.jpg')}}">
            </a>
            <div class="media-body">
                <a href="#">
                    {{$comment->user->name ?? '...'}}
                </a>
                <small class="text-muted">{{$comment->created_at->diffForHumans()}}</small>
            </div>
        </div>
        <div class="social-body">
            <p contentEditable="true">
                {{$comment->comment}}
            </p>
             {{--note by hatem if u wanna make it infinite replay just uncomment code below--}}
            {{--recursive idea--}}
{{--            @include('partials.comments', ['comments' => $comment->comments, 'post_id' => $post_id])--}}
            {{--@if(auth()->check())--}}
                {{--<form method="post" action="{{ route('addComment') }}">--}}
                    {{--@csrf--}}
                    {{--<div class="form-group">--}}
                        {{--<input type="text" name="comment" class="form-control" required/>--}}
                        {{--<input type="hidden" name="post_id" value="{{$row->id}}"/>--}}
                        {{--<input type="hidden" name="parent" value="{{$comment->id}}"/>--}}
                        {{--<input type="hidden" name="user_id" value="{{auth()->user()->id}}"/>--}}
                    {{--</div>--}}
                    {{--<div class="form-group">--}}
                        {{--<input type="submit" class="btn btn-primary" value="Add Replay"/>--}}
                    {{--</div>--}}
                {{--</form>--}}

            {{--@endif--}}
        </div>

    </div>

@endforeach

