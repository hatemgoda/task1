<?php

namespace App\Http\Controllers\Website;

use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    //

    public function addComment(Request $request)
    {
        //
//        return $request->all();

        $this->validate($request, [
//            'user_name' => 'required|unique:users,user_name',

            'comment' => 'required',


        ]);

//return  $images = $request->file('images');

        $row = Comment::create($request->all());
//        $client->roles()->sync($request->input('role'));

        toastr()->success('Save Done Successfully');
        return back();
    }

    public function deleteComment($id)
    {
        $row = Comment::where('id', $id)->where('user_id', auth()->user()->id)->first();
        if ($row) {
            $row->comments()->delete();
            $row->delete();

            toastr()->success('Delete  Done Successfully');
            return back();
        } else {
            toastr()->error('Error try again');
            return back();
        }

    }

    public function updateComment(Request $request)
    {
//        return $request->all();

        $row = Comment::where('id', $request->id)->where('user_id', auth()->user()->id)->first();
        if ($row) {
            $row->comment= $request->comment;
            $row->save();

            toastr()->success('Save  Done Successfully');
            return back();
        } else {
            toastr()->error('Error try again');
            return back();
        }

    }
}
