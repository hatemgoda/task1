<?php

namespace App\Http\Controllers\Website;

use App\City;


use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;


class PostController extends Controller
{
    //


    public function index(Request $request)
    {
        //
        return "aa";
        $rows = Post::latest();


        $rows = $rows->paginate(20);

        return view('website.post.index', compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Post $model)
    {
//        return "asa";
        return view('website.post.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        return $request->all();

        $this->validate($request, [

            'title' => 'required|max:255',
            'text' => 'required',


        ]);
        $request->merge(['user_id' => auth()->user()->id]);

        $row = Post::create($request->all());


        toastr()->success('Save Done Successfully');
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $row = Post::where('id', $id)->with('comments')->first();
//        return "dd";
        if ($row) {

            return view('website.post.show', compact('row'));
        }
        abort(404);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = Post::findOrFail($id);
        return view('website.post.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request, [

            'title' => 'required|max:255',
            'text' => 'required',


        ]);

        $row = Post::where('id', $id)->where('user_id', auth()->user()->id)->first();
        $row->update($request->all());


//        $client->roles()->sync($request->role);
        toastr()->success('Save Done Successfully');
        return redirect('/');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//return "ss";

        $row = Post::where('id', $id)->where('user_id', auth()->user()->id)->first();

        if ($row) {
            $row->allComments()->delete();


            $row->delete();

            toastr()->success('Delete  Done Successfully');
            return back();
        } else {
            toastr()->error('Error try again');
            return back();
        }


    }


}
