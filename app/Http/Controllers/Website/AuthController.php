<?php

namespace App\Http\Controllers\Website;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    //
    public function showLoginForm()
    {
//        dd(456);
        if (!auth()->check()) {
//            dd(567);
            return view('website.layouts.login');
        } else {
            return redirect('/');
        }
    }

    public function login(LoginRequest $request)
    {

//
        $user = User::where('email', $request->email)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                auth()->login($user);
                toastr()->success('Welcome Back');
                return redirect('/');
            } else {
                #wrong pass
                toastr()->error('Wrong password');
                return redirect()->back()->withInput($request->only('email'));
            }
        } else {
            #no user found
            toastr()->error('Wrong Email');
            return redirect()->back()->withInput($request->only('email'));
        }


    }

    public function showRegisterForm(User $model)
    {
        //
        if (!auth()->check()) {
            return view('website.layouts.register', compact('model'));
        } else {
            return redirect('/');
        }

    }

    public function register(RegisterRequest $request)
    {





        $request->merge(['password' => Hash::make($request->password)]);
        $row = User::create($request->except('password_confirmation'));
        if ($row) {
            toastr()->success('Save Done  Successfully');

            auth()->login($row);

            return redirect('/');
        }
        toastr()->error('Error try again');
        return back();

    }

    public function logout()
    {
        auth()->logout();

        return redirect('/');
    }




}
