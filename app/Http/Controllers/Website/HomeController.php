<?php

namespace App\Http\Controllers\Website;


use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    //
    public function index(Request $request)
    {

        $rows = Post::latest();
        if ($request->filled('title')) {

            $rows->where('title', 'like', '%' . $request->title . '%');
        }
        if ($request->filled('date')) {

            $rows->whereDate('created_at', $request->date);
        }
        if ($request->filled('user_id')) {

            $rows->where('user_id', $request->user_id);
        }
        $rows=$rows->paginate(9);
        return view('website.index', compact('rows'));
    }
}
