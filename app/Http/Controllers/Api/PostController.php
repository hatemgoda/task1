<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    //
    public function posts()
    {

        $rows = Post::latest()->paginate(20);
        if ($rows->count() > 0) {

            return PostResource::collection($rows)->additional(['status' => true]);
        }
        return response()->json(["status" => false, "message" => "no data"]);

    }


    public function post($id)
    {
//        $sliders = Slider::latest()->get();
        $row = Post::find($id);


        if ($row) {
            return (new PostResource($row))->additional(['status' => true]);
        }
        return response()->json(["status" => false, "message" => "no data"]);

    }

    public function savePost(Request $request)
    {
        $validation = validator()->make($request->all(), [
            'title' => 'required|max:255',
            'text' => 'required',
        ]);
        if ($validation->fails()) {
            $errors = $validation->errors();
            $error_data = [];
            foreach ($errors->all() as $error) {
                array_push($error_data, $error);
            }
            $data = $error_data;
            $response = [
                'status' => false,
                'error' => $data,
// 'msg' => 'من فضلك أدخل جميع الحقول وتأكد من صحة رقم الهاتف',
            ];
            return response()->json($response);
        }
        $request->merge(['user_id' => auth()->guard('api')->user()->id]);
        $row = Post::create($request->all());
        if ($row) {

            $data = [

                'status' => true,

                'message' => 'Save Done  Successfully',


            ];
            return response()->json($data, 200);
        } else {
            return response()->json([

                'status' => false,
                'error' => ['Error try again'],

            ], 200);
        }
    }

    public function updatePost(Request $request, $id)
    {

        $validation = validator()->make($request->all(), [
            'title' => 'required|max:255',
            'text' => 'required',

        ]);

        if ($validation->fails()) {
            $errors = $validation->errors();
            $error_data = [];
            foreach ($errors->all() as $error) {
                array_push($error_data, $error);
            }
            $data = $error_data;
            $response = [
                'status' => false,
                'error' => $data,

            ];
            return response()->json($response);
        }


        $row = Post::where('id', $id)->where('user_id', auth()->guard('api')->user()->id)->first();


        if ($row) {
            $row->update($request->all());

            $data = [

                'status' => true,

                'message' => 'Save Done  Successfully',


            ];
            return response()->json($data, 200);
        } else {
            return response()->json([

                'status' => false,
                'error' => ['Error try again'],

            ], 200);
        }
    }

    public function deletePost($id)
    {
//return "ss";
//        $row = Post::find($id);
        $row = Post::where('id', $id)->where('user_id', auth()->guard('api')->user()->id)->first();

        if ($row) {
            $row->allComments()->delete();


            $row->delete();
            $data = [

                'status' => true,

                'message' => 'Delete  Done Successfully',


            ];
            return response()->json($data, 200);

        } else {
            return response()->json([

                'status' => false,
                'error' => ['Error try again'],

            ], 200);
        }

    }
}
