<?php

namespace App\Http\Controllers\Api;

use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    //

    public function addComment(Request $request)
    {

        $validation = validator()->make($request->all(), [


            'comment' => 'required',
//            'user_id' => 'required|exists:users,id',
            'post_id' =>'required|exists:posts,id',
            'parent' => 'required',


        ]);
        if ($validation->fails()) {
            $errors = $validation->errors();
            $error_data = [];
            foreach ($errors->all() as $error) {
                array_push($error_data, $error);
            }
            $data = $error_data;
            $response = [
                'status' => false,
                'error' => $data,
// 'msg' => 'من فضلك أدخل جميع الحقول وتأكد من صحة رقم الهاتف',
            ];
            return response()->json($response);
        }
//return  $images = $request->file('images');

        $row = Comment::create([
            'comment' => $request->comment,
            'user_id' =>  auth()->guard('api')->user()->id,
            'post_id' => $request->post_id,
            'parent' =>  $request->parent,
        ]);
//        $client->roles()->sync($request->input('role'));
        if ($row) {

            $data = [

                'status' => true,

                'message' => 'Save Done  Successfully',


            ];
            return response()->json($data, 200);
        } else {
            return response()->json([

                'status' => false,
                'error' => ['Error try again'],

            ], 200);
        }
    }



    public function updateComment(Request $request)
    {


        $validation = validator()->make($request->all(), [


            'comment' => 'required',
            'id' => 'required',



        ]);
        if ($validation->fails()) {
            $errors = $validation->errors();
            $error_data = [];
            foreach ($errors->all() as $error) {
                array_push($error_data, $error);
            }
            $data = $error_data;
            $response = [
                'status' => false,
                'error' => $data,
// 'msg' => 'من فضلك أدخل جميع الحقول وتأكد من صحة رقم الهاتف',
            ];
            return response()->json($response);
        }

        $row = Comment::where('id', $request->id)->where('user_id', auth()->guard('api')->user()->id)->first();
        if ($row) {
            $row->comment= $request->comment;
            $row->save();

            $data = [

                'status' => true,

                'message' => 'Save Done  Successfully',


            ];
            return response()->json($data, 200);
        } else {
            return response()->json([

                'status' => false,
                'error' => ['Error try again'],

            ], 200);
        }

    }

    public function deleteComment($id)
    {

        $row = Comment::where('id', $id)->where('user_id', auth()->guard('api')->user()->id)->first();
        if ($row) {
            $row->comments()->delete();
            $row->delete();

            $data = [

                'status' => true,

                'message' => 'Delete  Done Successfully',


            ];
            return response()->json($data, 200);

        } else {
            return response()->json([

                'status' => false,
                'error' => ['Error try again'],

            ], 200);
        }

    }
}
