<?php

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->user_id == auth()->guard('api')->user()->id) {

            $permission = true;
        } else {

            $permission = false;
        }
        return [
            'id' => $this->id,
            'title' => $this->title,
            'text' => $this->text,
            'user_name' => $this->user->name ?? '...',
            'date' => $this->created_at->diffForHumans(),
            'can_update' => $permission,
            'comments' => CommentResource::collection($this->comments),

        ];
    }
}
