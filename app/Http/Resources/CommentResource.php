<?php

namespace App\Http\Resources;

use App\Comment;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->user_id == auth()->guard('api')->user()->id) {

            $permission = true;
        } else {

            $permission = false;
        }
        return [
            'id' => $this->id,
            'comment' => $this->comment,
            'user_name' => $this->user->name ?? '...',
            'date' => $this->created_at->diffForHumans(),
            'can_update' => $permission,
            'replies' => CommentResource::collection($this->comments),


        ];
    }
}
