<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Intervention\Image\Facades\Image;

class Comment extends Model
{
    //
    use SoftDeletes;

    protected $table = 'comments';


    public $timestamps = true;

    protected $guarded = ['id'];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);

    }
    public function comments()
    {
        return $this->hasMany(self::class,'parent');
    }



}
