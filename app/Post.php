<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Intervention\Image\Facades\Image;

class Post extends Model
{
    //
    use SoftDeletes;

    protected $table = 'posts';


    public $timestamps = true;

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function comments()
    {
        return $this->hasMany(Comment::class)->where('parent',0);
    }
    public function allComments()
    {
        return $this->hasMany(Comment::class);
    }
    public function getCommentsCountAttribute()
    {
        return $this->hasMany(Comment::class)->count();
    }

}
